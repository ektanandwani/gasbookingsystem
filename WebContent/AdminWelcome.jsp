<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome Admin</title>
<link rel="stylesheet" href="welcomeadm.css">
</head>
<body>

<header>
   <h1>Gas Booking System</h1>
</header>


<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a><br/>
  <a href="HelloAdm" target="iframe_a">Hello Admin</a>
  <a href="RequestedCyl" target="iframe_a">Requested Cylinders</a>
  <a href="PendingReq" target="iframe_a">Pending</a>
  <a href="GetTotalData" target="iframe_a">Reports</a>
</div>

<div id="main">
 <ul>
  <li><span style="color:white;font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span></li>
  <li><a class="active" href="#home"></a></li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">My Profile</a>
    <div class="dropdown-content">
      <a href="Logout.jsp">Log Out</a>
    </div>
  </li>
</ul>
<div class="p">

<iframe height="1000px" src="HelloAdm" name="iframe_a"></iframe>
</div>
</div>

<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}
</script>
</body>
</html>