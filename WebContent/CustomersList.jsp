<%@page import="beans.Requests"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import = "java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
<link rel = "stylesheet" href="styles.css">
</head>
<body>
<% List<Requests> listData= (List<Requests>)request.getAttribute("listOfCustomers"); %>
<% if(listData == null) {%>
No data Present.	
<% } else {%>
<table>
<thead>
<tr>
<th>Customer Id</th>
<th>Customer Name</th>
<th>Booking  Date (YYYY-MM-DD)</th>
<th>Delivery Date (YYYY-MM-DD)</th>
<th>Status</th>
</tr>
</thead>
<tbody>
<%
for (Requests customerData : listData){
%>
<tr>
<td width="119"><%=customerData.getCustomer().getCustId()%></td>
<td width="119"><%=customerData.getCustomer().getCustName()%></td>
<td width="119"><%=(Date)customerData.getBookingDate()%></td>
<td width="119"><%=(Date)customerData.getDeliveryDate()%></td>
<td width="119"><%=customerData.getStatus()%></td>
<tr>
<%} }%>
</tbody>
</table>
<br><br>
<a href="GetTotalData" style="text-decoration : none;"><- -Back</a>
</body>
</html>