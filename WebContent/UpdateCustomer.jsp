<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Updating Customer</title>
</head>
<body>
	<form action="UpdatedCustomer" method="post">
		<table>
			<tr>
				<td>Customer Name:</td>
				<td><input type="text" name="cname" value=${customer.custName}></td>
			</tr>
			<tr>
				<td>Customer Email:</td>
				<td><input type="email" name="cmail" value=${customer.custMail}></td>
			</tr>
			<tr>
				<td>Customer Address:</td>
				<td><textarea name=caddr>${customer.custAddr} </textarea></td>
			</tr>
			<tr>
				<td>Customer Password:</td>
				<td><input type="text" name="cpwd" value=${customer.custPwd}></td>
			</tr>
			<tr>
				<td>Customer Contact:</td>
				<td><input type="text" name="ccont" value=${customer.custContact} ></td>
			</tr>
			<tr>
				<td><input type="submit" value="Submit"></td>
				<td><input type="reset" value="Cancel"></td>
			</tr>
		</table>
	</form>
</body>
</html>