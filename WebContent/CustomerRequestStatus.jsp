<%@page import="java.util.ArrayList"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@page language="java" import="java.util.*"%>
<%@page import="beans.Requests"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Customer Requests</title>
</head>
<body>
	<table border="1">
		<tr>
			<th>Request Id</th>
			<th>Booking Date</th>
			<th>DeliveryDate</th>
			<th>Status</th>
		</tr>
		<%
			List<Requests> ar = (List<Requests>) request.getAttribute("rows");
			for (Requests r : ar) {
		%>
		<tr>
			<td><%=r.getId()%></td>
			<td><%=r.getBookingDate()%></td>
			<td><%=r.getDeliveryDate()%></td>
			<td>
				<%
					int status = r.getStatus();
						if (status == 0) {
							out.println("Your Request is not yet accepted");
						} else if (status == 1) {
							out.println("On the way");
						} else if (status == 2) {
							out.println("Delivered");
						} else if (status == 3) {
							out.println("Canceled");
						}
				%>
			</td>
		</tr>
		<%
			}
		%>

		</tr>
	</table>
</body>
</html>