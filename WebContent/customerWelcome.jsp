<!--<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>-->
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Welcome Customer</title>
<link rel="stylesheet" href="welcomeadm.css">

</head>
<body>

<header>
   <h1>Gas Booking System</h1>
</header>
<div id="mySidenav" class="sidenav">
  <a href="javascript:void(0)" class="closebtn" onclick="closeNav()">&times;</a>
  <a href="helloCustomer.jsp" target="iframe_a">Hello Customer</a>
  <a href="newBooking.jsp" target="iframe_a">Click to book</a>
  <a href="CancelRequest" target="iframe_a">Cancel request</a>
  <a href="CustRequestStatus" target="iframe_a">View request</a>
</div>


<div id="main">
 <ul>
  <li><span style="color:white;font-size:30px;cursor:pointer" onclick="openNav()">&#9776;</span></li>
  <li><a class="active" href="#home"></a></li>
  <li class="dropdown">
    <a href="javascript:void(0)" class="dropbtn">My Profile</a>
    <div class="dropdown-content">
	  <a href="UpdateCust" target="iframe_a">Edit Profile</a>
      <a href="logout.jsp">Log Out</a>
    </div>
  </li>
</ul>
<div>

<iframe height="1000px" width="100%" src="helloCustomer.jsp" name="iframe_a"></iframe>
</div>
</div>




<script>
function openNav() {
    document.getElementById("mySidenav").style.width = "250px";
    document.getElementById("main").style.marginLeft = "250px";
}

function closeNav() {
    document.getElementById("mySidenav").style.width = "0";
    document.getElementById("main").style.marginLeft= "0";
}
</script>
</body>
</html>