<%@page import="requests.GetTotalData"%>
<%@page import="java.time.Year"%>
<%@page import="java.time.Month"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<style>
body {
font-family : Century Gothic;
font-size : 18px;
}
tr,td,th {
	width : 30%;
	height : 30%;
}
td {
text-align : center; 
}
th {
border : 1px solid black;
}
</style>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<link href = "style.css" type = "text/css">
<title>Gas Booking system</title>
</head>
<body>
<div id = "filterRow">
<h4 style = >Filter By : </h4>
<form action="GetDataOfMonth">
Month : 
<select name = "month">
<option value = "01">January</option>
<option value = "02">February</option>
<option value = "03">March</option>
<option value = "04">April</option>
<option value = "05">May</option>
<option value = "06">June</option>
<option value = "07">July</option>
<option value = "08">August</option>
<option value = "09">September</option>
<option value = "10">October</option>
<option value = "11">November</option>
<option value = "12">December</option>
</select>
Year : 
<select name = "year">
<option value = "2015">2015</option>
<option value = "2016">2016</option>
<option value = "2017">2017</option>
</select>
<input type = "submit" value = "Search" >
</form>
<br>
<br>
<div style = "border : 2px solid black; width">
<h4>

<% String month = (String)request.getAttribute("monthName"); %>
<% String year = (String)request.getAttribute("year");%>

<% if(month == null && year == null) { %>
Total Requests
<% } else {%>
Month : <%=month%> Year : <%=year%>
<%} %>
</h4>
<table>
<tr>
<th>Type of Requests</th>
<th>Number of Requests</th>
</tr>
<tr>
<td>Requested</td>
<td><a href = "GetCustomersList?month=<%=month%>&year=<%=year%>&status=0"><%if(request.getAttribute("status0") == null) { %>0<% } else { %> 
<%=request.getAttribute("status0")%> <% } %></a></td>
</tr>
<tr>
<td>Pending</td>
<td><a href = "GetCustomersList?month=<%=month%>&year=<%=year%>&status=1"><%if(request.getAttribute("status1") == null) { %>0<% } else { %> 
<%=request.getAttribute("status1")%> <% } %></a></td>
</tr>

<tr>
<td>Delivered</td>
<td><a href = "GetCustomersList?month=<%=month%>&year=<%=year%>&status=2"><%if(request.getAttribute("status2") == null) { %>0<% } else { %> 
<%=request.getAttribute("status2")%> <% } %></a></td>
</tr>

<tr>
<td>Cancelled</td>
<td><a href = "GetCustomersList?month=<%=month%>&year=<%=year%>&status=3"><%if(request.getAttribute("status3") == null) { %>0<% } else { %> 
<%=request.getAttribute("status3")%> <% } %></a></td>
</tr>

</table>
</div>
<br>
<br>
<a href = "GetTotalData"><- -Back To Total Requests</a>
</div>
</body>
</html>