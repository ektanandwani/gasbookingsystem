package database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class DatabaseConnect {
	public Connection con = null;
	public static DatabaseConnect db;
	private DatabaseConnect(){
		String drivername = "com.mysql.jdbc.Driver";
		String url = "jdbc:mysql://localhost:3306/gasbooking";
		String username = "root";
		String password = "cybage@123";
		
		try{
			Class.forName(drivername);
		}
		catch(ClassNotFoundException e){
			e.printStackTrace();
		}
		try{
			this.con = DriverManager.getConnection(url,username,password);
		}
		catch(SQLException e){
			e.printStackTrace();
		}
	}
	public static synchronized DatabaseConnect connect(){
		if(db==null){
			db = new DatabaseConnect();
		}
		return db;
	}
}
