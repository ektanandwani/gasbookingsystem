/**
 * servlet for take data of requested cylinders
 * 
 * @author haritis
 *
 */
package adminservlets;

import java.io.IOException;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import database.DatabaseConnect;
import beans.Customer;
import beans.Requests;

/**
 * Servlet implementation class RequestedCyl
 */
@WebServlet("/RequestedCyl")
public class RequestedCyl extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public RequestedCyl() {
		super();

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseConnect db=DatabaseConnect.connect();
		ResultSet rs=null;
		try {

			Statement stmt = db.con.createStatement();
			String query = "SELECT requests.req_id,requests.booking_date,customer.cust_name FROM customer INNER JOIN requests ON requests.cust_id=customer.cust_id where requests.status=0";

			rs=stmt.executeQuery(query);

			ArrayList arr = new ArrayList<>();
			ArrayList cr = new ArrayList<>();

			while(rs.next()) {

				Requests r = new Requests();
				Customer c = new Customer();
				r.setId(rs.getInt("req_id"));
				r.setBookingDate(rs.getDate("booking_date"));
				c.setCustName(rs.getString("cust_name"));
				arr.add(r);
				cr.add(c);


			}

			request.setAttribute("requests",arr);
			request.setAttribute("cuts",cr);
			RequestDispatcher rd = request.getRequestDispatcher("requestedCyl.jsp");
			rd.forward(request, response);

		} catch( Exception ex ) {
			log( "Exception Occured", ex );
		}
		finally{
			try {
				if (rs != null) {
					rs.close();
				}
			} catch( Exception ex ) {
				log( "Exception during Resource.close()", ex );
			}}
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			doGet(request, response);}
		catch( Exception ex ) {
			log( "Exception Occured", ex );
		}
	}
}


