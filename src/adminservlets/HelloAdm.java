/**
 * Servlet for hello admin page
 * 
 * @author haritis
 *
 */
package adminservlets;

import java.io.IOException;

import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.DatabaseConnect;

import beans.Requests;

/**
 * Servlet implementation class RequestedCyl
 */
@WebServlet("/HelloAdm")
public class HelloAdm extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public HelloAdm() {
		super();

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseConnect db=DatabaseConnect.connect();
		ResultSet rs=null;
		try 
		{
			HttpSession session = request.getSession(false);
			Statement stmt = db.con.createStatement();
			String query = "SELECT requests.req_id FROM requests where requests.status=0";
			rs=stmt.executeQuery(query);
			ArrayList arr = new ArrayList<>();
			while(rs.next()) {
				Requests r = new Requests();
				r.setId(rs.getInt("req_id"));
				arr.add(r);
			}
			request.setAttribute("requests",arr);
			RequestDispatcher rd = request.getRequestDispatcher("helloAdm.jsp");
			rd.forward(request, response);

		} catch( Exception ex ) {
			log( "Exception Occured", ex );
		}
		finally{
			try {
				if (rs != null) {
					rs.close();
				}
			} catch( Exception ex ) {
				log( "Exception during Resource.close()", ex );
			}
		}
	}
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			doGet(request, response);}
		catch( Exception ex ) {
			log( "Exception Occured", ex );
		}
	}
}


