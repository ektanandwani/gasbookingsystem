/**
 * cancel feature servlet
 * 
 * @author haritis
 *
 */
package adminservlets;

import java.io.IOException;

import java.sql.PreparedStatement;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


import database.DatabaseConnect;

/**
 * Servlet implementation class CancelServlet
 */
@WebServlet("/CancelServlet")
public class CancelServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CancelServlet() {
		super();

	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");

		String id = request.getParameter("id");
		try {
			int i = Integer.parseInt(id);

			DatabaseConnect db=DatabaseConnect.connect();


			PreparedStatement ps=db.con.prepareStatement(  
					"update requests set status=3 where req_id=?");  

			ps.setInt(1,i); 

			ps.executeUpdate(); 

			RequestDispatcher rd = request.getRequestDispatcher("view.html");
			rd.include(request, response);



		}
		catch( Exception ex ) {
			log( "Exception Occured", ex );
		}
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try{
			doGet(request, response);}
		catch( Exception ex ) {
			log( "Exception Occured", ex );
		}
	}

}
