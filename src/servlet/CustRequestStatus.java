package servlet;

import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Requests;
import database.DatabaseConnect;

/**
 * Servlet implementation class CustRequestStatus
 */
@WebServlet("/CustRequestStatus")
public class CustRequestStatus extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CustRequestStatus() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseConnect db=DatabaseConnect.connect();
		HttpSession session=request.getSession(false);
		int cid= (int) session.getAttribute("custId");
		try {
			PreparedStatement ps=db.con.prepareStatement("select * from requests where cust_id=?");
			ps.setInt(1, cid);

			ResultSet rs=ps.executeQuery();
			ArrayList<Requests> rows = new ArrayList<>();

			while(rs.next()){
				Requests r= new Requests();
				r.setBookingDate(rs.getDate("booking_date"));
				r.setDeliveryDate(rs.getDate("delivery_date"));
				r.setStatus(rs.getInt("status"));
				r.setId(rs.getInt("req_id"));
				rows.add(r);
			}
			request.setAttribute("rows", rows);
			RequestDispatcher rd = request.getRequestDispatcher("CustomerRequestStatus.jsp");
			rd.forward(request, response);

		} catch (Exception e) {
			log(e.getMessage());
		}
	}

}
