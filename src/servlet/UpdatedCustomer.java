package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.DatabaseConnect;

/**
 * Servlet implementation class UpdatedCustomer
 */
@WebServlet("/UpdatedCustomer")
public class UpdatedCustomer extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdatedCustomer() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseConnect db=DatabaseConnect.connect();
		response.setContentType("text/html");
		PrintWriter out=response.getWriter();
		HttpSession session=request.getSession();
		int cid= (int) session.getAttribute("custId");
		String n=request.getParameter("cname");
		String e=request.getParameter("cmail");
		String a=request.getParameter("caddr");
		String p=request.getParameter("cpwd");
		Long c=Long.parseLong(request.getParameter("ccont"));
		try {
			PreparedStatement ps=db.con.prepareStatement("update customer set cust_name = ?,cust_mail = ?,cust_addr = ?,cust_contact = ?,cust_pwd = ? where cust_id = ?");
			ps.setString(1, n);
			ps.setString(2, e);
			ps.setString(3, a);
			ps.setLong(4, c);
			ps.setString(5, p);
			ps.setInt(6, cid);
			ps.executeUpdate();
			out.println("Successfully Updated your profile...");
			
		} catch (Exception e1) {
			log(e1.getMessage());
		}
	}

}
