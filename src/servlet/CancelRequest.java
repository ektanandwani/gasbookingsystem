package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Requests;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import database.DatabaseConnect;

/**
 * Servlet implementation class CancelRequest
 */
@WebServlet("/CancelRequest")
public class CancelRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CancelRequest() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		HttpSession session=request.getSession();
		int cid=(int) session.getAttribute("custId");
		DatabaseConnect db=DatabaseConnect.connect();
		int s=0;
		try {
			PreparedStatement ps=db.con.prepareStatement("Select * from requests where cust_id=? AND status=?");
			ps.setInt(1, cid);
			ps.setInt(2, s);
			ResultSet rs=ps.executeQuery();
			ArrayList<Requests> rows = new ArrayList<>();

			while(rs.next()){
				Requests r= new Requests();
				r.setId(rs.getInt("req_id"));
				rows.add(r);
			}
			request.setAttribute("rows", rows);
			RequestDispatcher rd = request.getRequestDispatcher("cancelRequest.jsp");
			rd.forward(request, response);
			
		} catch (SQLException e) {
			log(e.getMessage());
		}
	}

}
