package servlet;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Customer;

import java.sql.*;


import database.DatabaseConnect;

/**
 * Servlet implementation class UpdateCust
 */
@WebServlet("/UpdateCust")
public class UpdateCust extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public UpdateCust() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		DatabaseConnect db=DatabaseConnect.connect();
		try {
			String cName;
			String cEmail;
			String cAddr;
			long cCont;
			String cPwd;
			Customer c=new Customer();
			HttpSession session=request.getSession();
			int cid= (int) session.getAttribute("custId");
			PreparedStatement ps=db.con.prepareStatement("Select * from customer where cust_id=?");
			ps.setInt(1, cid);
			ResultSet rs=ps.executeQuery();
			if (rs.next()) {

				cName = rs.getString("cust_name");
				cEmail= rs.getString("cust_mail");
				cAddr= rs.getString("cust_addr");
				cCont= rs.getLong("cust_contact");
				cPwd= rs.getString("cust_pwd");
				c.setCustId(cid);
				c.setCustName(cName);
				c.setCustAddr(cAddr);
				c.setCustContact(cCont);
				c.setCustPwd(cPwd);
				c.setCustMail(cEmail);
			}

			request.setAttribute("customer", c);
			RequestDispatcher rd = request.getRequestDispatcher("UpdateCustomer.jsp");
			rd.forward(request, response);

		} catch (SQLException e) {
			log(e.getMessage());
		}
	}

}
