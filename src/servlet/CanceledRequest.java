package servlet;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import database.DatabaseConnect;

/**
 * Servlet implementation class CanceledRequest
 */
@WebServlet("/CanceledRequest")
public class CanceledRequest extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public CanceledRequest() {
		super();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session=request.getSession(false);
		PrintWriter out=response.getWriter();
		int id =(int) session.getAttribute("custId") ;
		int s = 3;
		int req1=Integer.parseInt(request.getParameter("req"));
		DatabaseConnect db=DatabaseConnect.connect();
		try {
			PreparedStatement ps=db.con.prepareStatement("update requests set status=? where req_id=? AND cust_id=?");  
			ps.setInt(1,s);
			ps.setInt(2,req1);
			ps.setInt(3, id);
			ps.executeUpdate();
			out.println("Your request has been Canceled...");
		}
		catch(Exception e)
		{
			log(e.getMessage());
		}
	}

}
