package servlet;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.DriverManager;
import java.time.LocalDate;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.jdbc.Connection;
import com.mysql.jdbc.Driver;
import com.mysql.jdbc.PreparedStatement;
import beans.Customer;
import database.DatabaseConnect;

/**
 * Servlet implementation class BookGass
 */
@WebServlet("/BookGass")
public class BookGass extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public BookGass() {
		super();

	}


	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		HttpSession session = request.getSession(false);
		int customerId = (int)session.getAttribute("custId");
		response.setContentType("text/html");
		Customer c = new Customer();
		c.setCustId(2);
		LocalDate presentDate = LocalDate.now();
		LocalDate deliveryDate = presentDate.plusDays(7);
		try {
			Class.forName("com.mysql.jdbc.Driver");
		} catch (ClassNotFoundException e1) {
			log(e1.getMessage());;
		}
		try {
			DatabaseConnect db = DatabaseConnect.connect();
			String insertQuery="insert into requests(booking_date,delivery_date,status,cust_id) values(?,?,?,?)";
			PreparedStatement ps=(PreparedStatement) db.con.prepareStatement(insertQuery,PreparedStatement.RETURN_GENERATED_KEYS);
			ps.setDate(1, java.sql.Date.valueOf(presentDate));
			ps.setDate(2, java.sql.Date.valueOf(deliveryDate));
			ps.setInt(3, 0);
			ps.setInt(4, customerId);
			ps.executeUpdate();
			response.sendRedirect("SuccessBook.jsp");
		}
		catch(Exception e)
		{
			log(e.getMessage());
		}
	}
}
