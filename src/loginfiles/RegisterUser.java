package loginfiles;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import beans.Customer;
@WebServlet("/RegisterUser")
public class RegisterUser extends HttpServlet {
	private static final long serialVersionUID = 1L;
    public RegisterUser() {
        super();
    }

    @Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    	response.setContentType("text/html");
    	PrintWriter out = response.getWriter();
		RequestDispatcher rd;
		Customer customer = new Customer();
		customer.setCustName(request.getParameter("username"));
		customer.setCustAddr(request.getParameter("address"));
		customer.setCustContact(Long.parseLong(request.getParameter("contact")));
		customer.setCustPwd(request.getParameter("password"));
		customer.setCustMail(request.getParameter("usermail"));
		
		String retypePassword = request.getParameter("retype_password");

		try{
			if(customer.getCustPwd().equals(retypePassword))
			{
				boolean registered;
				registered = DatabaseOperations.registerUser(customer);
				if(registered) {
					out.print("<html><body>");
					out.print("Registration Successful");
					out.println("<a href = \"index.html\">Move to LoginPage</a>");
					out.println("</body></html>");
				}
			}
			else {
					rd = request.getRequestDispatcher("UserRegister.html");
					rd.include(request, response);
			}
		}
		catch (Exception e){
			log("Exception occured" + e);
		}
	}
}
