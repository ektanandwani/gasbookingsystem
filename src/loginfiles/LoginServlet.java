package loginfiles;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import beans.Admin;
import beans.Customer;

@WebServlet("/LoginServlet")
public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	HttpSession session;
       
    public LoginServlet() {
        super();
    }

	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		int isAdmin;
		isAdmin = checkAdminOrCustomer(request);
		String usermail = request.getParameter("usermail");
		String password = request.getParameter("password");
		
		try {
			if(isAdmin == 1) {
				Admin admin;
				admin = DatabaseOperations.adminValidate(usermail, password);
				if(admin == null){
					invalidateLogin(request,response);
				}
				else {
					login(request, response, admin);
				}
			}
			else {
				Customer customer;
				customer = DatabaseOperations.customerValidate(usermail, password);
				if(customer == null) {
					invalidateLogin(request,response);
				}
				else {
					login(request, response, customer);
				}
			}
		}
		 catch (SQLException e) {
				e.printStackTrace();
			}
	}


	private void login(HttpServletRequest request, HttpServletResponse response , Admin admin) throws IOException, ServletException {
		session = request.getSession();
		session.setAttribute("adminId", admin.getAdminId());
		session.setAttribute("adminName", admin.getAdminName());
		RequestDispatcher rd = request.getRequestDispatcher("AdminWelcome.jsp");
		//System.out.println(session.getAttribute("adminName"));
		rd.forward(request, response);
		//response.sendRedirect("AdminWelcome.jsp");
		
	}
	private void login(HttpServletRequest request, HttpServletResponse response , Customer customer) throws IOException, ServletException {
		HttpSession session;
		session = request.getSession();
		session.setAttribute("custId", customer.getCustId());
		session.setAttribute("custName", customer.getCustName());
		RequestDispatcher rd = request.getRequestDispatcher("CustomerWelcome2.html");
		rd.forward(request, response);
		
	}

	private void invalidateLogin(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
		PrintWriter out = response.getWriter();
		RequestDispatcher rd = request.getRequestDispatcher("index.html");
		rd.include(request, response);
		out.println("Invalid Credentials.");
	}

	private int checkAdminOrCustomer(HttpServletRequest request) {
		String user = request.getParameter("user");
		if(user.equals("admin"))
			return 1;
		else
			return 0;
	}

}
