/**
 * @author Ekta Nandwani
 */
package loginfiles;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import beans.Admin;
import beans.Customer;
import database.DatabaseConnect;

public class DatabaseOperations {
	
	public static Customer customerValidate(String usermail, String password) throws SQLException{
		
		DatabaseConnect db = DatabaseConnect.connect();
		Statement st = db.con.createStatement();
		ResultSet rs;
		String customerQuery = "select cust_id, cust_name, cust_pwd from customer where cust_mail = '" + usermail + "'";
		rs = st.executeQuery(customerQuery);
			
		
		while(rs.next()) {
			String userPassword = rs.getString("cust_pwd");
			if(password.equals(userPassword)) {
				Customer customer = new Customer();
				customer.setCustId(rs.getInt("cust_id"));
				customer.setCustName(rs.getString("cust_name"));
				return customer;
			}
		}
		rs.close();
		return null;
	}
	
	public static Admin adminValidate(String usermail, String password) throws SQLException{
		
		DatabaseConnect db = DatabaseConnect.connect();
		Statement st = db.con.createStatement();
		ResultSet rs;
		String adminQuery = "select admin_id, admin_name, admin_pwd from admin where admin_mail = '" + usermail + "'";
		rs = st.executeQuery(adminQuery);
		
		while(rs.next()) {
			String userPassword = rs.getString("admin_pwd");
			if(password.equals(userPassword)){
				Admin admin = new Admin();
				admin.setAdminId(rs.getInt("admin_id"));
				admin.setAdminName(rs.getString("admin_name"));
				return admin;
			}
		}
		rs.close();
		return null;
	}
	
	public static boolean registerUser(Customer c) throws SQLException {

		DatabaseConnect db = DatabaseConnect.connect();
		Statement st = db.con.createStatement();
		String insertQuery = "INSERT INTO CUSTOMER(cust_name, cust_mail, cust_addr, cust_contact, cust_pwd)"
				+ " values(\'"+ c.getCustName() + "\',\'" + c.getCustMail() + "\',\'" + c.getCustAddr() + "\',"
				+ c.getCustContact() + ",\'" + c.getCustPwd() + "\')";
		
		int result = st.executeUpdate(insertQuery);
		if (result != 0)
			return true;
		return false;
		
		
		
	}
}
