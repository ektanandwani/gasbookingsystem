/**
 * @author Deval Bhagat
 */
package requests;

import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import beans.Customer;
import beans.Requests;
import database.DatabaseConnect;

/**

 * @author Deval Bhagat 
 *
 * 
 */
public class RequestDAO {
	DatabaseConnect db = DatabaseConnect.connect();
	Map<String, Integer> hm = new HashMap<>();
	
	public Map<String, Integer> getTotalData() throws SQLException{
		ResultSet rs = null;
		try {
			String query = "select * from requests";
			Statement st = db.con.createStatement();
			rs = st.executeQuery(query);
			int count0 = 0;
			int count1 = 0;
			int count2 = 0;
			int count3 = 0;
			while (rs.next()) {
				int status = rs.getInt("status");
				if(status==0){
					count0 = count0 + 1;
					hm.put("requested", count0);
				}
				else if(status==1){
					count1 = count1 + 1;
					hm.put("pending", count1);
				}
				else if(status==2){
					count2 = count2 + 1;
					hm.put("delivered", count2);
				}
				else if(status==3) {
					count3 = count3 + 1;
					hm.put("cancelled", count3);
				}
			}
		} catch (SQLException e) {
			e.getMessage();
		}
		finally {
			rs.close();
		}
		return hm;
	}

	public Map<String, Integer> getFilterData(int month , int year) throws SQLException{
		CallableStatement statement = db.con.prepareCall("call filterData(?,?)");
		statement.setInt(1, month);
		statement.setInt(2, year);
		ResultSet rs = statement.executeQuery();
		while(rs.next()) {
			int count = rs.getInt("count(*)");
			int status = rs.getInt("status");
			if(status == 0) {
				hm.put("requested", count);
			}
			else if(status == 1) {
				hm.put("pending", count);
			}
			else if(status == 2) {
				hm.put("delivered", count);
			}
			else if(status == 3) {
				hm.put("cancelled", count);
			}
		}
		return hm;
	}
	
	public List<Requests> filterData(int month, int year, int status) throws SQLException {
		ResultSet rs = null ;
		ArrayList<Requests> arr = new ArrayList<>();
		if(month == 0 && year == 0) {
			String query = "select * from requests r inner join customer c where r.cust_id = c.cust_id and status = "+status+"";
			Statement st = db.con.createStatement();
			rs = st.executeQuery(query);
		}
		else {
			CallableStatement st = db.con.prepareCall("call getCustomerListByMonth(?,?,?)");
			st.setInt(1, month);
			st.setInt(2, year);
			st.setInt(3, status);
			rs = st.executeQuery();
		} 
		while(rs.next()) {
			Customer c = new Customer();
			c.setCustName(rs.getString("cust_name"));
			c.setCustId(rs.getInt("cust_id"));
			Requests req = new Requests();
			req.setStatus(rs.getInt("status"));
			req.setCustomer(c);
			req.setBookingDate(rs.getDate("booking_date"));
			req.setDeliveryDate(rs.getDate("delivery_date"));
			arr.add(req);
		}
		return arr;
	}
}
