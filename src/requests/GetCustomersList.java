package requests;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import beans.*;

@WebServlet("/GetCustomersList")
public class GetCustomersList extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetCustomersList() {
		super();
	}
	public void exception(Exception e) {
		log("Exception occurred",e);
	}

	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String month = request.getParameter("month");
		String year = request.getParameter("year");
		List<Requests> list = new ArrayList<>();
		RequestDAO rd = new RequestDAO();
		int status = Integer.parseInt(request.getParameter("status"));
		if(month.equals("null")){
			try {
				list = rd.filterData(0, 0, status);
			} catch (SQLException e) {
				log("Exception", e);
			}
		}
		else {
			try {
				list = rd.filterData(Month.valueOf(request.getParameter("month")).getValue(), Integer.parseInt(year), status);
			}
			catch (Exception e) {
				exception(e);
			}
		}
		if(list.isEmpty()) {
			try {
				PrintWriter out = response.getWriter();
				out.println("No data present!");
				RequestDispatcher rdo = request.getRequestDispatcher("CustomersList.jsp");
				rdo.forward(request, response);
			}
			catch(Exception e) {
				exception(e);
			}
		}
		else {
			try {
				request.setAttribute("listOfCustomers", list);
				RequestDispatcher dispatcher = request.getRequestDispatcher("CustomersList.jsp");
				dispatcher.forward(request, response);
			}
			catch(Exception e) {
				exception(e);
			}
		}
	}
}


