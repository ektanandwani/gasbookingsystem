package requests;

import java.io.IOException;
import java.time.Month;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/GetDataOfMonth")
public class GetDataOfMonth extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetDataOfMonth() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try {
			int month = Integer.parseInt(request.getParameter("month"));
			int year = Integer.parseInt(request.getParameter("year"));
			RequestDAO functions = new RequestDAO();
			Map<String, Integer> hashmap;
			hashmap = functions.getFilterData(month, year);
			request.setAttribute("monthName", Month.of(month).name());
			request.setAttribute("year", Integer.toString(year));
			request.setAttribute("status0", hashmap.get("requested"));
			request.setAttribute("status1", hashmap.get("pending"));
			request.setAttribute("status2", hashmap.get("delivered"));
			request.setAttribute("status3", hashmap.get("cancelled"));
			RequestDispatcher rdis = request.getRequestDispatcher("ViewReports.jsp");
			rdis.forward(request, response);
		} 
		catch (Exception e) {
			log("Exception", e);
		}
	}
}
