package requests;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;;

@WebServlet("/GetTotalData")
public class GetTotalData extends HttpServlet {
	private static final long serialVersionUID = 1L;

	public GetTotalData() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		RequestDAO functions = new RequestDAO();
		Map<String, Integer> hashmap = new HashMap<>();
		try {
			hashmap = functions.getTotalData();
		} catch (SQLException e1) {
			log("Exception", e1);
		}
		request.setAttribute("status0", hashmap.get("requested"));
		request.setAttribute("status1", hashmap.get("pending"));
		request.setAttribute("status2", hashmap.get("delivered"));
		request.setAttribute("status3", hashmap.get("cancelled"));
		try {
			RequestDispatcher dispatcher = request.getRequestDispatcher("ViewReports.jsp");
			dispatcher.forward(request, response);
		}
		catch (Exception e) {
			log("Exception", e);
		}
	}

}
