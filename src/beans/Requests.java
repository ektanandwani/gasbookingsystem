/**
 * added toString method
 * 
 * @author haritis
 *
 */
package beans;
import beans.Customer;
import java.util.Date;

public class Requests {
	private int id;
	private Customer customer;
	private Date bookingDate;
	private Date deliveryDate;
	private int status;
	Customer cus = new Customer();
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Customer getCustomer() {
		return customer;
	}
	public void setCustomer(Customer customer) {
		this.customer = customer;
	}
	public Date getBookingDate() {
		return bookingDate;
	}
	public void setBookingDate(Date bookingDate) {
		this.bookingDate = bookingDate;
	}
	public Date getDeliveryDate() {
		return deliveryDate;
	}
	public void setDeliveryDate(Date deliveryDate) {
		this.deliveryDate = deliveryDate;
	}
	public int getStatus() {
		return status;
	}
	public void setStatus(int status) {
		this.status = status;
	}
	public String getCustName()
	{
		return cus.getCustName();
	}
	@Override
	public String toString()
	{
		return Integer.toString(id); 
	}
}
