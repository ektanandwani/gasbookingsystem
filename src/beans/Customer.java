/**
 * added toString method
 * 
 * @author haritis
 *
 */
package beans;

public class Customer {
	private int custId;
	private String custName;
	private String custAddr;
	private String custMail;
	private String custPwd;
	private long custContact;
	public int getCustId() {
		return custId;
	}
	public void setCustId(int custId) {
		this.custId = custId;
	}
	public String getCustName() {
		return custName;
	}
	public void setCustName(String custName) {
		this.custName = custName;
	}
	public String getCustAddr() {
		return custAddr;
	}
	public void setCustAddr(String custAddr) {
		this.custAddr = custAddr;
	}
	public String getCustMail() {
		return custMail;
	}
	public void setCustMail(String custMail) {
		this.custMail = custMail;
	}
	public String getCustPwd() {
		return custPwd;
	}
	public void setCustPwd(String custPwd) {
		this.custPwd = custPwd;
	}
	public long getCustContact() {
		return custContact;
	}
	public void setCustContact(long custContact) {
		this.custContact = custContact;
	}
	@Override
	public String toString()
	{
		return  custName; 
	}
	
	
	
}
